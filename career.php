<?php

interface work {

    public function experience($designation, $url, $company, $location, $duration);
}

interface education {

    public function qualification($tests);
}

interface career extends work, education {

    public function skills();
}



?>